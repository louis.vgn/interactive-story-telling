# Launch the Game # 

In order to launch the game it’s simple: 
1. Download the whole repository from here: https://gitlab.com/louis.vgn/interactive-story-telling/-/tree/master
2. Go to your web browser.
3. Open a window from a file (`ctrl + o`).
4. Make sure you have the sound up.
5. Select the `index.html` launch the game. 
6. Enjoy. 
